# Импортируем необходимые библиотеки для нашего приложения
import numpy as np
import catboost
from flask import Flask, request, render_template

app = Flask(__name__)
export FLASK_APP=example
export FLASK_ENV=development


# Загружаем модель и определяем параметры функции  -  будущие входы для модели (всего 12 параметров)

def set_params(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12):
    model = model_catboost.load_model('Models/catboost_model')
    preds = model.model_catboost.predict([p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12])
    return preds[0][0]

@app.route('/', methods=['post', 'get'])

def app_calculation():
    param_lst = []
    message = ''
    if request.method == 'POST':
        
       # получим данные из наших форм и кладем их в список, который затем передадим функции set_params
        for i in range(1,13,1):
            p = request.form.get(f'p{i}')
            param_lst.append(float(p))
            
        message = set_params(*param_lst)

    # указываем шаблон и прототип сайта для вывода    
    return render_template("/templates/index.html", message=message) 

# Запускаем приложение  
app.run()